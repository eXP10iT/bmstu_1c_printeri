﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	//{{_КОНСТРУКТОР_ПЕЧАТИ(ПечатьСоздатьЗаявкуНаЗаправкуДокумент)
	ТабДок = Новый ТабличныйДокумент;
	ПечатьСоздатьЗаявкуНаЗаправкуДокумент(ТабДок, ПараметрКоманды);

	ТабДок.ОтображатьСетку = Ложь;
	ТабДок.Защита = Ложь;
	ТабДок.ТолькоПросмотр = Ложь;
	ТабДок.ОтображатьЗаголовки = Ложь;
	ТабДок.Показать();
	//}}
КонецПроцедуры

&НаСервере
Процедура ПечатьСоздатьЗаявкуНаЗаправкуДокумент(ТабДок, ПараметрКоманды)
	Документы.СоздатьЗаявкуНаЗаправкуДокумент.ПечатьСоздатьЗаявкуНаЗаправкуДокумент(ТабДок, ПараметрКоманды);
КонецПроцедуры
